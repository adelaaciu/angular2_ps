import { CevatyePage } from './app.po';

describe('cevatye App', () => {
  let page: CevatyePage;

  beforeEach(() => {
    page = new CevatyePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
