/**
 * Created by Adela on 23.05.2017.
 */
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';


const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class LogoutService {

  constructor(private http: Http) {
  }

  logout() {
    return this.http.get(baseUrl + '/logout');
  }
}
