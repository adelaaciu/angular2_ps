import {Component} from '@angular/core';
import {LogoutService} from './logout.service';


@Component({
  selector: 'app-logout-component',
  template: `

    <button (click)="logout()" class="btn btn-primary">Logout</button>
  `,
  styles: [`
    .form {
      width: 500px;
    }
  `],
  providers: [LogoutService]
})
export class LogoutComponent {
  name: string;
  password: string;
  role: string;

  constructor(private logoutService: LogoutService) {
  }

  logout() {
    this.logoutService.logout().subscribe(() => localStorage.setItem('userBtoa', null));
  }

  register() {

  }
}
