import {Component} from '@angular/core';
import {Hospital, HospitalService} from './hospital.service';
@Component({
  selector: 'app-hospital-component',
  template: `
    Search hospital by city
    <br/>
    <input [(ngModel)]="city" type="text">
    <button (click)="getHospitalByCity()" class="btn btn-primary">Search</button>

    <table id="t04" class="table table-bordered" [mfData]="hospitals" #mf="mfDataTable" [mfRowsOnPage]="10">
      <thead>
      <tr>
        <th style="width: 10%">
          <mfDefaultSorter by="name">Name</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="city">City</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="doctors">Doctors</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="sectors">Sectors</mfDefaultSorter>
        </th>

      </tr>
      </thead>

      <tbody>
      <tr template='ngFor let e of hospitals'>
        <td>{{e.name}}</td>
        <td>{{e.city}}</td>
        <td>
          <div *ngFor='let doctor of e.doctorList'>
            {{doctor.name}}
          </div>
        </td>
        <td>
          <div *ngFor='let sector of e.sectorList'>
            {{sector.name}}
          </div>
        </td>
      </tr>
      </tbody>

      <tfoot>
      <tr>
        <td colspan="20">
          <mfBootstrapPaginator [rowsOnPageSet]="[5,10,25]"></mfBootstrapPaginator>
        </td>
      </tr>
      </tfoot>
    </table>


  `,
  providers: [HospitalService]
})
export class HospitalComponent {
  city: string;
  hospitals: Hospital[];

  constructor(private hospitalService: HospitalService) {
  }

  getHospitalByCity() {
    this.hospitalService.getHospitalsByCity(this.city).subscribe(
      hospitals => {
        this.hospitals = hospitals;
        console.log(hospitals);
      }
    );
  }

  exportListOfHospitals() {

  }
}
