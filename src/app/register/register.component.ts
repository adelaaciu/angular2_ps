import {Component} from '@angular/core';
import {RegisterService} from './register.service';


@Component({
  selector: 'app-register-component',
  template: `
    <div class="col-md-2 col-md-offset-5">
      UserName:
      <input [(ngModel)]="name" type="text">
      Password:
      <input [(ngModel)]="password" type="password">
      Email:
      <input [(ngModel)]="email" type="email">
      <button (click)="register()" class="btn btn-primary">Register</button>
    </div>
  `,
  providers: [RegisterService]
})
export class RegisterComponent {
  name: string
  password: string
  email: string
  role: string

  constructor(private registerService: RegisterService) {
  }

  login() {
  }

  register() {
    this.registerService.register(this.name, this.password).subscribe(data => console.log(data));
  }
}
