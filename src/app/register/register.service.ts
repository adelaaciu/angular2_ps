import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {User} from '../_shared/user';


const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class RegisterService {

  constructor(private http: Http) {
  }

  register(username, password) {
    const headers = new Headers({
      'Content-Type': 'application/json'
    });
    const options = new RequestOptions( {headers: headers});

    const user = new User(username, password);
    return this.http.post(baseUrl + '/users/create', user, {headers: headers});
  }
}
