import {Component, OnInit} from "@angular/core";
import {Appointment, AppointmentService} from "./appointment.service";
import {Router} from "@angular/router";
import {isNull, isUndefined} from "util";

@Component({
  selector: 'app-appointment-component',
  template: `
    DoctorName
    <br/>
    <input [(ngModel)]="doctorName" type="text">
    <br/>
    Hopital
    <br/>
    <input [(ngModel)]="hospital" type="text">
    <br/>
    HospitalCity
    <br/>
    <input [(ngModel)]="hospitalCity" type="text">
    <br/>
    Category
    <br/>
    <input [(ngModel)]="category" type="text">
    <br/>
    Date
    <br/>
    <input [(ngModel)]="date" type="text">
    <br/>

    <button type="button" (click)="makeAppointment()" class="btn btn-warning">Make appointment</button>

  `,
  providers: [AppointmentService]
})
export class AppointmentComponent implements OnInit {
  doctorName: string;
  hospital: string;
  category: string;
  hospitalCity;
  date;

  constructor(private appointmentService: AppointmentService, private router: Router) {
  }

  ngOnInit(): void {
    if (isUndefined(localStorage.getItem('userBtoa')) || isNull(localStorage.getItem('userBtoa')) || localStorage.getItem('userBtoa') === "null") {
      this.router.navigate(['/login']);
    }
  }

  makeAppointment() {
    const appointment: Appointment = new Appointment();
    appointment.doctorName = this.doctorName;
    appointment.hospitalName = this.hospital;
    appointment.hospitalCity = this.hospitalCity;
    appointment.category = this.category;
    appointment.date = this.date;

    this.appointmentService.makeAppointment(appointment).subscribe(app => {
      console.log(app);
    });
  }

}
