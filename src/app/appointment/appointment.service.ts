import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import "rxjs/add/operator/map";

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class AppointmentService {

  hello = 'heo';


  constructor(private http: Http) {
  }

  makeAppointment(appointment: Appointment) {


    const headers = new Headers({
      'Authorization': 'Basic ' + localStorage.getItem('userBtoa'),
      'X-Requested-With': 'XMLHttpRequest',
      'Content-type': 'application/json'
    });
    return this.http.post(baseUrl + '/appointments/create/', JSON.stringify(appointment),
      {
        headers: headers
      }
    ).map(res => res.json());
  }
}


export class Appointment {
  doctorName;
  hospitalName;
  category;
  hospitalCity;
  date;

  constructor() {

  }
}
