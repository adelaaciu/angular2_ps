import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class PharmacyService {

  constructor(private http: Http) {
  }

  getPharmacyByCity(city) {
    return this.http.get(baseUrl + '/pharmacies?city=' + city).map(res => res.json());
  }
}

export interface  Pharmacy {
  name;
  startHour;
  endHour;
  city;
  address;
}
