import {Component} from "@angular/core";
import {Pharmacy, PharmacyService} from "./pharmacy.service";

@Component({
  selector: 'app-pharmacy-component',
  template: `
    Search pharmacy by city
    <br/>
    <input [(ngModel)]="city" type="text">
    <button (click)="getPharmacyByCity()" class="btn btn-primary">Search</button>


    <table id="t05" class="table table-bordered" [mfData]="pharmacies" #mf="mfDataTable" [mfRowsOnPage]="10">

      <thead>
      <tr>
        <th style="width: 10%">
          <mfDefaultSorter by="name">Name</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="startHour">StartHour</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="endHour">EndHour</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="city">City</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="address">Address</mfDefaultSorter>
        </th>

      </tr>
      </thead>

      <tbody>
      <tr template='ngFor let e of pharmacies'>
        <td>{{e.name}}</td>
        <td>{{e.startHour}}</td>
        <td>{{e.endHour}}</td>
        <td>{{e.city}}</td>
        <td>{{e.address}}</td>
      </tr>
      </tbody>

      <tfoot>
      <tr>
        <td colspan="20">
          <mfBootstrapPaginator [rowsOnPageSet]="[5,10,25]"></mfBootstrapPaginator>
        </td>
      </tr>
      </tfoot>
    </table>

    <div class="btn-group" dropdown>
      <button dropdownToggle type="button" class="btn btn-primary dropdown-toggle">
        Export <span class="caret"></span>
      </button>
      <ul *dropdownMenu class="dropdown-menu" role="menu">
        <li role="menuitem"><a href="http://localhost:8080/pharmacies/getfile/csv" class="dropdown-item">CSV</a></li>
        <li role="menuitem"><a href="http://localhost:8080/pharmacies/getfile/json" class="dropdown-item">Json</a></li>
      </ul>
    </div>

  `,
  providers: [PharmacyService]
})
export class PharmacyComponent {
  city: string;
  pharmacies: Pharmacy[];
  pharmaciesFormat;

  constructor(private pharmacyService: PharmacyService) {
  }

  getPharmacyByCity() {
    this.pharmacyService.getPharmacyByCity(this.city).subscribe(
      pharmacies => this.pharmacies = pharmacies,
      pharmacies => console.log(pharmacies)
    );
  }
}
