import {Component, OnInit} from '@angular/core';
import {Drug, DrugService} from './drug.service';

@Component({
  selector: 'app-drug-component',
  template: `
    <!--<button (click)='getDrugs()' class='btn btn-primary'>Get Drugs</button>-->

    <span class="list-group">
      <th style="width: 40%"><button type="button" class="list-group-item list-group-item-success"
                                     (click)="getDrugsByCategory('inima')">Inima</button>  </th>
      <br/>
      <th style="width: 30%"><button type="button" class="list-group-item list-group-item-info"
                                     (click)="getDrugsByCategory('plamani')">Plamani</button></th>
      <br/>
      <th style="width: 20%"> <button type="button" class="list-group-item list-group-item-warning"
                                      (click)="getDrugsByCategory('stomac')">Stomac</button></th>
      <br/> 
      <th style="width: 10%"> <button type="button" class="list-group-item list-group-item-danger"
                                      (click)="getDrugsByCategory('ochi')">Ochi</button></th>
    </span>

    <table id="t03" class="table table-bordered" [mfData]="drugList" #mf="mfDataTable" [mfRowsOnPage]="10">
      <thead>
      <tr>
        <th style="width: 20%">
          <mfDefaultSorter by="name">Name</mfDefaultSorter>
        </th>
        <th>
          <mfDefaultSorter by="composition">Composition</mfDefaultSorter>
        </th>
        <th>
          <mfDefaultSorter by="category">Category</mfDefaultSorter>
        </th>
        <th>
          <mfDefaultSorter by="adverseReaction">AdverseReaction</mfDefaultSorter>
        </th>
        <th>
          <mfDefaultSorter by="diseases">Diseases</mfDefaultSorter>
        </th>

      </tr>
      </thead>

      <tbody>
      <tr *ngFor='let drug of drugList'>
        <td>{{drug.name}}</td>
        <td>{{drug.composition}}</td>
        <td>{{drug.category}}</td>
        <td>{{drug.adverseReactions}}</td>
        <td>
          <div *ngFor='let dis of drug.diseases'>
            {{dis.name}}
          </div>
        </td>
        </tr>

      </tbody>

      <tfoot>
      <tr>
        <td colspan="20">
          <mfBootstrapPaginator [rowsOnPageSet]="[5,10,25]"></mfBootstrapPaginator>
        </td>
      </tr>
      </tfoot>
    </table>
    
  `,
  providers: [DrugService]
})
export class DrugsComponent implements OnInit {

  // drugs: string;
  drugList: Drug[];

  ngOnInit(): void {
    this.drugService.getDrugs().subscribe(drugList => {
      this.drugList = drugList;
      console.log(drugList);
    });
  }

  constructor(private drugService: DrugService) {
  }

  getDrugByName() {
  }

  getDrugsByCategory(categorie) {
    this.drugService.getDrugsByCategory(categorie).subscribe(drugList => this.drugList = drugList);
  }
}
