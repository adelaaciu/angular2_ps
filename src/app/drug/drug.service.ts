import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Disease} from '../disease/disease.service';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class DrugService {

  constructor(private http: Http) {
  }

  getDrugs() {
    console.log('getDrugs');
    return this.http.get(baseUrl + '/drugs/list').map(res => res.json());
  }

  getDrugByName(name) {
    return this.http.get(baseUrl + '/drugs/' + name).map(res => res.json());
  }

  getDrugsByCategory(category) {
    console.log(category);
    return this.http.get(baseUrl + '/drugs/category/' + category).map(res => res.json());
  }
}

export interface Drug {
  name;
  composition;
  category;
  adverseReaction: string[];
  diseases: Disease[];
}
