export interface Sector {
  name;
  level;
  chambers;
  NO_MAX_OF_PERSONS;
}
