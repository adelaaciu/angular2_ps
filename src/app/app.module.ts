import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import {NavbarComponent} from './navbar/navbar.component';
import {RouterModule, RouterOutlet, RouterOutletMap, Routes} from '@angular/router';
import {DrugsComponent} from './drug/drug.component';
import {DiseaseComponent} from './disease/disease.component';
import {DoctorComponent} from './doctor/doctor.component';
import {DataTableModule} from 'angular2-datatable';
import {HospitalComponent} from './hospital/hospital.component';
import {PharmacyComponent} from './pharmacy/pharmacy.component';
import {LoginComponent} from './login/login.component';
import {AppointmentComponent} from './appointment/appointment.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import {RegisterComponent} from 'app/register/register.component';
import {LogoutComponent} from './logout/logout.component';


const appRoutes: Routes = [
  {path: 'drugs', component: DrugsComponent},
  {path: 'diseases', component: DiseaseComponent},
  {path: 'doctors', component: DoctorComponent},
  {path: 'hospitals', component: HospitalComponent},
  {path: 'pharmacies', component: PharmacyComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'appointments', component: AppointmentComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DrugsComponent,
    DiseaseComponent,
    DoctorComponent,
    HospitalComponent,
    PharmacyComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    AppointmentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    RouterModule.forRoot(appRoutes),
    BsDropdownModule.forRoot()
  ],
  providers: [RouterOutletMap],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
