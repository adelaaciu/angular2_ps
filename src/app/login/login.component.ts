import {Component} from "@angular/core";
import {LoginService} from "./login.service";
import {Router} from '@angular/router';


@Component({
  selector: 'app-login-component',
  template: `
    <div>
      <!--<div class="form">-->

      <!--<div class="input-group input-group-lg">-->
      <!--<span class="input-group-addon" id="sizing-addon1">Username</span>-->
      <!--<input class="form-control" id="sizing-addon2" [(ngModel)]="name" type="text">-->
      <!--</div>-->

      <!--<div class="input-group input-group-lg">-->
      <!--<span class="input-group-addon" id="sizing-addon1">Password&nbsp;</span>-->
      <!--<input class="form-control" id="sizing-addon2" [(ngModel)]="password" type="password" name="password">-->
      <!--</div>-->

      <!--<button (click)="login()" class="btn btn-primary">Login</button>-->
      <!---->
      <!--</div>-->

      <form role="form" (ngSubmit)="login()">
        <div class="form-group">
          <label for="username">Username:</label>
          <input type="text" class="form-control" id="username" name="username" [(ngModel)]="name"/>
        </div>
        <div class="form-group">
          <label for="password">Password:</label>
          <input type="password" class="form-control" id="password" name="password" [(ngModel)]="password"/>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  `,
  styles: [`
    .form {
      width: 500px;
    }
  `],
  providers: [LoginService]
})
export class LoginComponent {
  name: string;
  password: string;

  constructor(private loginService: LoginService, private router: Router) {
  }

  login() {
    this.loginService.login(this.name, this.password).subscribe((data) => {
      localStorage.setItem('userBtoa', btoa(this.name + ':' + this.password));
      this.router.navigate(['/appointments']);
      console.log(data);
    });
  }
}
