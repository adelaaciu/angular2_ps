import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import 'rxjs/add/operator/map';


const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class LoginService {

  constructor(private http: Http) {
  }

  login(username, password) {
    const headers = new Headers({
      'Authorization': 'Basic ' + btoa(username + ':' + password),
      'X-Requested-With' : 'XMLHttpRequest'
    });
    return this.http.get(baseUrl + '/log', {headers: headers});
  }
}
