import {Component} from "@angular/core";
import {Disease, DiseaseService} from "./disease.service";

@Component({
  selector: 'app-disease-component',
  template: `
    Disease Name:
    <input [(ngModel)]="diseaseName" type="text">
    <button (click)="getDiseaseByName()" class="btn btn-default btn-sm">Get Disease</button>


    <table id="t01" class="table table-bordered" [mfData]="diseases" #mf="mfDataTable" [mfRowsOnPage]="10">
      <thead>
      <tr>
        <th style="width: 10%">
          <mfDefaultSorter by="name">Name</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="symptoms">Symptoms</mfDefaultSorter>
        </th>

      </tr>
      </thead>

      <tbody>
      <tr template='ngFor let e of diseases'>
        <td>{{e.name}}</td>
        <td>
          {{e.symptomList}}
      </tr>
      </tbody>

      <tfoot>
      <tr>
        <td colspan="20">
          <mfBootstrapPaginator [rowsOnPageSet]="[5,10,25]"></mfBootstrapPaginator>
        </td>
      </tr>
      </tfoot>
    </table>

    <div class="btn-group" dropdown>
      <button dropdownToggle type="button" class="btn btn-primary dropdown-toggle">
        Choose category <span class="caret"></span>
      </button>
      <ul *dropdownMenu class="dropdown-menu" role="menu">
        <li role="menuitem"><a class="dropdown-item" (click)="getDiseaseByCategory('inima')">Inima</a></li>
        <li role="menuitem"><a class="dropdown-item" (click)="getDiseaseByCategory('plamani')">Plamani</a></li>
        <li role="menuitem"><a class="dropdown-item" (click)="getDiseaseByCategory('stomac')">Stomac</a></li>
        <li role="menuitem"><a class="dropdown-item" (click)="getDiseases()">All</a>
        </li>
      </ul>
    </div>

  `,
  providers: [DiseaseService]
})
export class DiseaseComponent {
  diseaseName: string;
  diseases: {};

  constructor(private diseaseService: DiseaseService) {
  }

  getDiseaseByName() {
    this.diseaseService.getDiseaseByName(this.diseaseName).subscribe(diseases => {
        this.diseases = diseases;
        console.log(diseases);
      }
    );
  }

  getDiseaseByCategory(category) {
    console.log(category);
    this.diseaseService.getDiseaseByCategory(category).subscribe(diseases => {
      this.diseases = diseases;
      console.log(diseases);
    });
  }

  getDiseases() {
    console.log('1');
    this.diseaseService.getDiseases().subscribe(diseases => this.diseases = diseases);
  }

}
