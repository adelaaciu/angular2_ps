import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class DiseaseService {
  constructor(private http: Http) {
  }

  getDiseaseByName(name) {
    return this.http.get(baseUrl + '/diseases?name=' + name).map(res => res.json());
  }

  getDiseaseByCategory(category) {
    return this.http.get(baseUrl + '/diseases/category/' + category).map(res => res.json());
  }

  getDiseases() {
    return this.http.get(baseUrl + '/diseases/list')
      .map(res => res.json());
  }
}

export interface Disease {
  name;
  symptoms: string[];
}
