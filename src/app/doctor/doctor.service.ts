import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

const ip = 'http://localhost';
const port = '8080';
const baseUrl = ip + ':' + port;

@Injectable()
export class DoctorService {

  constructor(private http: Http) {
  }

  getDoctorsByHospital(hospital, city) {
    return this.http.get(baseUrl + '/doctors/' + hospital + '/' + city).map(res => res.json());
  }

  getAllDoctors() {
    return this.http.get(baseUrl + '/doctors/list').map(res => res.json());
  }
}

export interface Doctor {
  name;
  phoneNo;
  startHour;
  endHour;
}

