import {Component, OnInit} from '@angular/core';
import {Doctor, DoctorService} from './doctor.service';

@Component({
  selector: 'app-doctor-component',
  template: `
    Search doctor by hospital
    <br/>
    Hospital name:
    <br/>
    <input [(ngModel)]="hospital" type="text">
    <br/>
    City:
    
    <br/>
    <input [(ngModel)]="city" type="text">
    <br/>
    <button (click)="getDoctorsByHospital()" class="btn btn-success">Get doctors</button>
    <br/>

    <table id="t03" class="table table-bordered" [mfData]="doctors" #mf="mfDataTable" [mfRowsOnPage]="10">
      <thead>
      <tr>
        <th style="width: 10%">
          <mfDefaultSorter by="firstName">Name</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="lastName">PhoneNo</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="firstName">StartHour</mfDefaultSorter>
        </th>
        <th style="width: 10%">
          <mfDefaultSorter by="lastName">EndHour</mfDefaultSorter>
        </th>
      </tr>
      </thead>

      <tbody>
      <tr template='ngFor let e of doctors'>
        <td>{{e.name}}</td>
        <td>{{e.phoneNo}}</td>
        <td>{{e.startHour}}</td>
        <td>{{e.endHour}}</td>
      </tr>
      </tbody>

      <tfoot>
      <tr>
        <td colspan="20">
          <mfBootstrapPaginator [rowsOnPageSet]="[5]"></mfBootstrapPaginator>
        </td>
      </tr>
      </tfoot>
    </table>

  `,
  providers: [DoctorService],
})
export class DoctorComponent {
  hospital: string;
  doctorName: string;
  city: string;
  doctors: Doctor[];

  constructor(private doctorService: DoctorService) {
  }

  getDoctorsByHospital() {
    this.doctorService.getDoctorsByHospital(this.hospital, this.city).subscribe(data => {
      this.doctors = data;
    });
  }
}
